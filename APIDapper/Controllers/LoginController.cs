﻿using Domain.Interfaces.Services;
using Domain.Models;
using Domain.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace API.Controllers
{
    /// <summary>
    /// Classe para login no sistema
    /// </summary>
    [DisableRequestSizeLimit]
    [Route("blue/[controller]/[action]")]
    [ApiController]
    public class LoginController(IConfiguration configuration, IServices services) : ControllerBase
    {
        private readonly IConfiguration _configuration = configuration;
        private readonly IServices _services = services;

        /// <summary>
        /// Método efetuar Login no sistema
        /// </summary>
        /// <returns>Objeto com dados de acesso</returns>
        /// <response code="200">Retorna os dados de acesso</response>
        /// <response code="204">Sem conteudo</response>
        /// <response code="400">Problemas com o model enviado</response>
        /// <response code="500">Internal server error</response>
        [AllowAnonymous]
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> CheckLogin([FromBody] LoginIn objeto)
        {
            if (!ModelState.IsValid)
                return BadRequest(string.Join("\r\n", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

            try
            {
                return Ok(await _services.CheckLogin(objeto.Login??string.Empty, objeto.Password??string.Empty));
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
