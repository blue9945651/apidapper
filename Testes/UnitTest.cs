using Domain.Interfaces.Services;
using Domain.Models.DTOs;
using System.ComponentModel.DataAnnotations;

namespace Testes
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            
        }

        [TestCase(0, 1, "Teste", "teste@teste.com.br", "11987678987")]
        [TestCase(1, 0, "Teste", "teste@teste.com.br", "11987678987")]
        [TestCase(0, 1, null, "teste@teste.com.br", "11987678987")]
        [TestCase(0, 1, null, null, "11987678987")]
        [TestCase(0, 1, null, null, null)]
        [Test]
        public void ValidateAgendaData(int id, int logiInd, string Nome, string Email, string Telefone)
        {
            Agenda agenda = new()
            {
                Email = Email,
                Id = id,
                LoginId = logiInd,
                Nome = Nome,
                Telefone = Telefone
            };
            CheckModel(agenda);
            Assert.Pass();
        }

        private static void CheckModel(object Objeto)
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(Objeto, null, null);
            if (!Validator.TryValidateObject(Objeto, context, results))
                throw new Exception((string.Join(string.Empty, results.SelectMany(v => v.ErrorMessage))));
        }
    }
}