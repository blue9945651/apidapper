﻿using Domain.Interfaces.Repositories;
using Domain.Interfaces.Services;
using Domain.Models;
using Domain.Models.DTOs;
using Domain.Utils;
using Microsoft.Extensions.Configuration;

namespace Services
{
    public class Services(IRepository repository, IConfiguration configuration) : IServices
    {
        private readonly IRepository _repository = repository;
        private readonly IConfiguration _configuration = configuration;

        public async Task<LoginReturn> CheckLogin(string Login, string Password)
        {
            var returnCheck = await _repository.CheckLogin(Login, Password);

            var loginReturn = new LoginReturn()
            {
                IsSucess = returnCheck.Any()
            };

            if (!returnCheck.Any())
            {
                loginReturn.ErrorMessage = "Usuário e/ou senha inválido(s)";
            }
            else
            {
                loginReturn.ID = returnCheck.FirstOrDefault().ID;
                loginReturn.NOME = returnCheck.FirstOrDefault()?.NOME;
                loginReturn.EMAIL = returnCheck.FirstOrDefault()?.EMAIL;
                loginReturn.LOGIN = returnCheck.FirstOrDefault()?.LOGIN;
                loginReturn.TOKEN = Token.GerarTokenJwt(_configuration, returnCheck.FirstOrDefault().ID, returnCheck.FirstOrDefault()?.NOME, returnCheck.FirstOrDefault()?.EMAIL);
            }

            return loginReturn;
        }

        public async Task<IEnumerable<Agenda>> GetAllAgenda(int ID)
        {
            return await _repository.GetAllAgenda(ID);
        }

        public async Task<IEnumerable<Agenda>> GetAgenda(int ID)
        {
            return await _repository.Get(ID);
        }

        public async Task<int> CreateAgenda(Agenda objeto)
        {
            //Validar email minimo com @
            if (!objeto.Email.Contains('@'))
                throw new Exception("Formato do email é inválido");

            //Validar duplicação conforme NOME ou EMAIL
            var checkIfEmailExists = await _repository.GetFromEmail(objeto);

            if (checkIfEmailExists.Any())
                throw new Exception("Emáil ou Nome já encontra-se cadastrado");

            return await _repository.CreateAgenda(objeto);
        }

        public async Task<int> UpdateAgenda(Agenda objeto)
        {
            //Validar email minimo com @
            if (!objeto.Email.Contains('@'))
                throw new Exception("Formato do email é inválido");

            //Validar duplicação conforme NOME ou EMAIL
            var checkIfEmailExists = await _repository.GetFromEmailInsteadThis(objeto);

            if (checkIfEmailExists.Any())
                throw new Exception("Já existe um outro contato com esse mesmo nome ou email");

            return await _repository.UpdateAgenda(objeto);
        }

        public async Task<int> DeleteAgenda(int ID)
        {
            return await _repository.DeleteAgenda(ID);
        }
    }
}
