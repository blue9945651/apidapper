﻿using System.Security.Cryptography;
using System.Text;

namespace Domain.Utils
{
    public class Criptografia
    {
        public static string EncryptPwd(string pwd)
        {
            byte[] pwdToHash = StringToByteArray(pwd);
            byte[] pwdHashValue = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(pwdToHash);
            return BitConverter.ToString(pwdHashValue);
        }

        private static Byte[] StringToByteArray(String s)
        {
            return (new UnicodeEncoding()).GetBytes(s);
        }
    }
}
