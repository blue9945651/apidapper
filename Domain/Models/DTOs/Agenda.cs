﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models.DTOs
{
    public class Agenda
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Usuário deve ser informado")]
        public int LoginId { get; set; }

        [Required(ErrorMessage = "Nome deve ser informado")]
        [MaxLength(100, ErrorMessage = "O tamanho máximo do Nome é 100")]
        [MinLength(3, ErrorMessage = "O tamanho minimo do Nome é 3")]
        public string? Nome { get; set; }

        [Required(ErrorMessage = "Nome deve ser informado")]
        [MaxLength(150, ErrorMessage = "O tamanho máximo do Email é 150")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Telefone deve ser informado")]
        [MaxLength(50, ErrorMessage = "O tamanho máximo do Telefone é 50")]
        public string? Telefone { get; set; }
    }
}
