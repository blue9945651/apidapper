﻿namespace Domain.Models
{
    public class LoginReturn
    {
        public int ID { get; set; }
        public string? NOME { set; get; }
        public string? EMAIL { set; get; }
        public string? LOGIN { set; get; }
        public string? TOKEN { set; get; }
        public bool IsSucess { get; set; }
        public string? ErrorMessage { get; set; }
    }
}
