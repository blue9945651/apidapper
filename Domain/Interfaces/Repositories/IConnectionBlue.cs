﻿using System.Data;

namespace Domain.Interfaces.Repositories
{
    public interface IConnectionBlue
    {
        IDbConnection DbConnection { get; }
    }
}
