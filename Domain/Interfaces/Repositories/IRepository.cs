﻿using Domain.Models;
using Domain.Models.DTOs;

namespace Domain.Interfaces.Repositories
{
    public interface IRepository
    {
        Task<IEnumerable<Login>> CheckLogin(string Login, string Password);
        Task<IEnumerable<Agenda>> GetAllAgenda(int ID);
        Task<IEnumerable<Agenda>> Get(int ID);
        Task<int> CreateAgenda(Agenda objeto);
        Task<IEnumerable<Agenda>> GetFromEmail(Agenda objeto);
        Task<IEnumerable<Agenda>> GetFromEmailInsteadThis(Agenda objeto);
        Task<int> UpdateAgenda(Agenda objeto);
        Task<int> DeleteAgenda(int ID);
    }
}
