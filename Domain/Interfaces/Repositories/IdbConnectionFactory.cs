﻿using Domain.Enums;
using System.Data;

namespace Domain.Interfaces.Repositories
{
    public interface IdbConnectionFactory
    {
        IDbConnection CreateDBConnection(DBConnectionName connectionaName);
    }
}
