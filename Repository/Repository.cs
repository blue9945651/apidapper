﻿using Dapper;
using Domain.Interfaces.Repositories;
using Domain.Models.DTOs;
using Domain.Utils;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Repository
{
    public class Repository : IRepository
    {
        private readonly IConfiguration _confinguration;
        private readonly IConnectionBlue _connectionBlue;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);
        private IDbConnection? DbConnection { get; set; }

        public Repository(IConfiguration confinguration, IConnectionBlue connectionBlue)
        {
            _confinguration = confinguration;
            _connectionBlue = connectionBlue;
            AddDbConnection();
        }

        private void AddDbConnection()
        {
            DbConnection = _connectionBlue.DbConnection;
        }

        public async Task<IEnumerable<Login>> CheckLogin(string Login, string Password)
        {
            try
            {
                ConfigConnection();

                string sql = @"SELECT
                                *
                            FROM
                                LOGIN WITH (NOLOCK)
                            WHERE
                                (LOGIN = @LOGIN OR EMAIL = @EMAIL)
                                AND SENHA = @SENHA
                        ";

                var parameters = new DynamicParameters();
                parameters.Add("@LOGIN", Login, DbType.String, ParameterDirection.Input);
                parameters.Add("@EMAIL", Login, DbType.String, ParameterDirection.Input);
                parameters.Add("@SENHA", Criptografia.EncryptPwd(Password), DbType.String, ParameterDirection.Input);

                return await DbConnection.QueryAsync<Login>(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public async Task<IEnumerable<Agenda>> GetAllAgenda(int ID)
        {
            try
            {
                ConfigConnection();

                string sql = @"SELECT
                                *
                            FROM
                                AGENDA WITH (NOLOCK)
                            WHERE
                                LOGIN_ID = @LOGIN_ID
                            ORDER BY NOME
                        ";

                var parameters = new DynamicParameters();
                parameters.Add("@LOGIN_ID", ID, DbType.Int32, ParameterDirection.Input);

                return await DbConnection.QueryAsync<Agenda>(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public async Task<IEnumerable<Agenda>> Get(int ID)
        {
            try
            {
                ConfigConnection();

                string sql = @"SELECT
                                *
                            FROM
                                AGENDA WITH (NOLOCK)
                            WHERE
                                ID = @ID
                        ";

                var parameters = new DynamicParameters();
                parameters.Add("@ID", ID, DbType.Int32, ParameterDirection.Input);

                return await DbConnection.QueryAsync<Agenda>(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public async Task<IEnumerable<Agenda>> GetFromEmail(Agenda objeto)
        {
            try
            {
                ConfigConnection();

                string sql = @"SELECT
                                *
                            FROM
                                AGENDA WITH (NOLOCK)
                            WHERE
                                LOGIN_ID = @LOGIN_ID
                                AND (EMAIL = @EMAIL OR NOME = @NOME)
                        ";

                var parameters = new DynamicParameters();
                parameters.Add("@LOGIN_ID", objeto.LoginId, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@EMAIL", objeto.Email, DbType.String, ParameterDirection.Input);
                parameters.Add("@NOME", objeto.Nome, DbType.String, ParameterDirection.Input);

                return await DbConnection.QueryAsync<Agenda>(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public async Task<IEnumerable<Agenda>> GetFromEmailInsteadThis(Agenda objeto)
        {
            try
            {
                ConfigConnection();

                string sql = @"SELECT
                                *
                            FROM
                                AGENDA WITH (NOLOCK)
                            WHERE
                                ID != @ID
                                AND (EMAIL = @EMAIL OR NOME = @NOME)
                        ";

                var parameters = new DynamicParameters();
                parameters.Add("@ID", objeto.Id, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@EMAIL", objeto.Email, DbType.String, ParameterDirection.Input);
                parameters.Add("@NOME", objeto.Nome, DbType.String, ParameterDirection.Input);

                return await DbConnection.QueryAsync<Agenda>(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public async Task<int> CreateAgenda(Agenda objeto)
        {
            try
            {
                ConfigConnection();

                string sql = @"INSERT AGENDA (
                                    LOGIN_ID,
                                    NOME,
                                    EMAIL,
                                    TELEFONE
                                ) VALUES (
                                    @LOGIN_ID,
                                    @NOME,
                                    @EMAIL,
                                    @TELEFONE
                                );
                        ";

                var parameters = new DynamicParameters();
                parameters.Add("@LOGIN_ID", objeto.LoginId, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@NOME", objeto.Nome, DbType.String, ParameterDirection.Input);
                parameters.Add("@EMAIL", objeto.Email, DbType.String, ParameterDirection.Input);
                parameters.Add("@TELEFONE", objeto.Telefone, DbType.String, ParameterDirection.Input);

                return await DbConnection.ExecuteAsync(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public async Task<int> UpdateAgenda(Agenda objeto)
        {
            try
            {
                ConfigConnection();

                string sql = @"UPDATE AGENDA SET
                                NOME = @NOME,
                                EMAIL = @EMAIL,
                                TELEFONE = @TELEFONE
                               WHERE 
                                ID = @ID
                            ";

                var parameters = new DynamicParameters();
                parameters.Add("@ID", objeto.Id, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@NOME", objeto.Nome, DbType.String, ParameterDirection.Input);
                parameters.Add("@EMAIL", objeto.Email, DbType.String, ParameterDirection.Input);
                parameters.Add("@TELEFONE", objeto.Telefone, DbType.String, ParameterDirection.Input);

                return await DbConnection.ExecuteAsync(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public async Task<int> DeleteAgenda(int ID)
        {
            try
            {
                ConfigConnection();

                string sql = @"DELETE AGENDA WHERE ID = @ID";

                var parameters = new DynamicParameters();
                parameters.Add("@ID", ID, DbType.Int32, ParameterDirection.Input);

                return await DbConnection.ExecuteAsync(sql, parameters);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        private void ConfigConnection()
        {
            if (DbConnection.State.Equals(ConnectionState.Closed))
                DbConnection.Open();
        }

        private void CloseConnection()
        {
            if (DbConnection.State.Equals(ConnectionState.Open))
                DbConnection.Close();
        }
    }
}
