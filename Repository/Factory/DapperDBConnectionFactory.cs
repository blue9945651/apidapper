﻿using Domain.Enums;
using Domain.Interfaces.Repositories;
using System.Data;
using System.Data.SqlClient;

namespace Repository.Factory
{
    public class DapperDBConnectionFactory : IdbConnectionFactory
    {
        private readonly IDictionary<DBConnectionName, string> _connectionDict;

        public DapperDBConnectionFactory(IDictionary<DBConnectionName, string> connectionDict)
        {
            _connectionDict = connectionDict;
        }

        public IDbConnection CreateDBConnection(DBConnectionName connectionaName)
        {
            string connectionString = string.Empty;

            if (_connectionDict.TryGetValue(connectionaName, out connectionString))
            {
                return new SqlConnection(connectionString);
            }

            throw new ArgumentException();
        }
    }
}
