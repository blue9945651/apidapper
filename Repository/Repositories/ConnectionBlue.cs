﻿using Domain.Interfaces.Repositories;
using System.Data;

namespace Repository.Repositories
{
    public class ConnectionBlue : IConnectionBlue
    {
        public IDbConnection DbConnection { get; private set; }

        public ConnectionBlue(IdbConnectionFactory idbConnectionFactory)
        {
            DbConnection = idbConnectionFactory.CreateDBConnection(Domain.Enums.DBConnectionName.ConnectionBlue);
        }
    }
}
